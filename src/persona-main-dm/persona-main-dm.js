import { LitElement, html } from 'lit';

class PersonaMainDm extends LitElement{

    static get properties(){
      return {
        people: {type: Array}
      };
    }

    constructor(){
      super();

      this.people = [
        {
          name:"Gustavo Sierra",
          yearsInCompany: 10,
          photo: {
            src: "./img/persona1.png",
            alt: "Gustavo Sierra"
          },
          profile: "Perfil 1, define lo que es el perfil"
        } , {
          name:"Miguel Garcia",
          yearsInCompany: 2,
          photo: {
            src: "./img/persona2.png",
            alt: "Miguel Garcia"
          },
          profile: "Perfil 2, segundo comentario"
        } , {
          name:"Juan Tenorio",
          yearsInCompany: 5,
          photo: {
            src: "./img/persona3.png",
            alt: "Juan Tenorio"
          },
          profile: "Perfil 3, tercer modo de lo que hay que mostrar"
        } , {
          name:"Juana Perez",
          yearsInCompany: 2,
          photo: {
            src: "./img/persona7.png",
            alt: "Juana Perez"
          },
          profile: "Perfil 4, es el penúltimo falta algo más"
        } , {
          name:"Almudena Barriga",
          yearsInCompany: 2,
          photo: {
            src: "./img/persona8.png",
            alt: "Almudena Barriga"
          },
          profile: "Perfil 5, solo faltaba este, punto final"
        }
      ]
    }

    updated(changedProperties){
      console.log("updated en persona-dm");
      console.log(changedProperties);

      if (changedProperties.has("people")){
        console.log("Ha cambiado el valor de la propiedad people en persona-dm");

        this.dispatchEvent(
          new CustomEvent(
            "chargePeople",
            {
              detail: {
                people: this.people
              }
            }
          )
        )

      }
    }




}

customElements.define("persona-main-dm", PersonaMainDm);
