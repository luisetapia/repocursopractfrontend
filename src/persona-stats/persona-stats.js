import { LitElement, html } from 'lit';

class PersonaStats extends LitElement{

    static get properties(){
      return {
        people: {type: Array}
      };
    }

    constructor(){
      super();

      this.people = [];
    }

    //el changeProperties, tiene los valores anteriores
    updated(changedProperties){
      console.log("updated en persona-stats");
      console.log(changedProperties);

      if (changedProperties.has("people")){
        console.log("Ha cambiado el valor de la propiedad people en persona-stats");

        let peopleStats = this.gatherPeopleArrayInfo(this.people);

        this.dispatchEvent(
          new CustomEvent(
            "updated-people-stats",
            {
              detail: {
                peopleStats: peopleStats
              }
            }
          )
        )

      }
    }

    gatherPeopleArrayInfo(people){
      console.log("gatherPeopleArrayInfo");

      let peopleStats = {};
      peopleStats.numberOfPeople = people.length;

      console.log(peopleStats);

      return peopleStats;
    }

    //Quitamos el render, este componente solo se dedica a obtener datos, como un dataManager
    /*render(){
        return html`
          <h1>Persona Stats</h1>
      `
    }*/

}

customElements.define("persona-stats", PersonaStats);
