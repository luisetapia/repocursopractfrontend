import { LitElement, html } from 'lit';

class FichaPersona extends LitElement{

    static get properties(){
      return {
        name: {type: String},
        yearsInCompany: {type: Number},
        personInfo: {type: String}
      }
    }

    constructor() {
      super();

      this.name = "Carlos";
      this.yearsInCompany = 4;

      //this.devuelvePersonInfo();
   }

    //updated recibe un parámetro map
    updated(changedProperties) {

      changedProperties.forEach(
        (oldValue, propName) => {
          console.log("Propiedad " + propName +
          " ha cambiado de valor, anterior era " + oldValue)
        }
      )

      if(changedProperties.has("name")){
        console.log("Propiedad name cambia valor, anterior era "
        + changedProperties.get("name")+" nuevo es " + this.name)
      }

      if(changedProperties.has("yearsInCompany")){
        console.log("Propiedad yearsInCompany cambia valor, anterior era "
        + changedProperties.get("yearsInCompany")+" nuevo es " + this.yearsInCompany);

        this.devuelvePersonInfo();
      }
    }

    render(){
        return html`
          <div>
              <label>Nombre Completo</label>
              <input type="text" id="fname" value="${this.name}" @input="${this.updateName}"></input>
              <br />
              <label>Años en la empresa</label>
              <input type="text" value="${this.yearsInCompany}" @input="${this.updatePersonInfo}"></input>
              <br />
              <input type="text" value="${this.personInfo}" disabled></input>
              <br />
          </div>
      `
    }

    //@change se ejecuta cuando se pierde el foco
    updateName(e){
        console.log("updateName");

        console.log(e);
        this.name = e.target.value;
    }

    updatePersonInfo(e){
      console.log("updatePersonInfo");

      this.yearsInCompany = e.target.value;
    }

    devuelvePersonInfo(){
      console.log("propiedad yearInCompany vale " + this.yearsInCompany);
      if(this.yearsInCompany >= 7){
        this.personInfo = "lead";
      } else if(this.yearsInCompany >= 5){
        this.personInfo = "senior";
      } else if(this.yearsInCompany >= 3){
        this.personInfo = "team";
      } else {
        this.personInfo = "junior";
      }
    }

}

customElements.define("ficha-persona", FichaPersona);
