import { LitElement, html } from 'lit';
import '../persona-ficha-listado/persona-ficha-listado.js'
import '../persona-form/persona-form.js'
import '../persona-main-dm/persona-main-dm.js'

class PersonaMain extends LitElement{

    static get properties(){
      return {
        people: {type: Array},
        showPersonForm: {type: Boolean}
      };
    }

    constructor(){
      super();

      this.people = [];
      /*this.people = [
        {
          name:"Gustavo Sierra",
          yearsInCompany: 10,
          photo: {
            src: "./img/persona1.png",
            alt: "Gustavo Sierra"
          },
          profile: "Perfil 1, define lo que es el perfil"
        } , {
          name:"Miguel Garcia",
          yearsInCompany: 2,
          photo: {
            src: "./img/persona2.png",
            alt: "Miguel Garcia"
          },
          profile: "Perfil 2, segundo comentario"
        } , {
          name:"Juan Tenorio",
          yearsInCompany: 5,
          photo: {
            src: "./img/persona3.png",
            alt: "Juan Tenorio"
          },
          profile: "Perfil 3, tercer modo de lo que hay que mostrar"
        } , {
          name:"Juana Perez",
          yearsInCompany: 2,
          photo: {
            src: "./img/persona7.png",
            alt: "Juana Perez"
          },
          profile: "Perfil 4, es el penúltimo falta algo más"
        } , {
          name:"Almudena Barriga",
          yearsInCompany: 2,
          photo: {
            src: "./img/persona8.png",
            alt: "Almudena Barriga"
          },
          profile: "Perfil 5, solo faltaba este, punto final"
        }
      ]*/

      this.showPersonForm = false;
    }

    render(){
        return html`
          <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">

          <h2 class="text-center">Personas</h2>

          <div class="row" id="peopleList">
            <main class="row row-cols-1 row-cols-sm-4">
                ${this.people.map(
                    person => html`<persona-ficha-listado
                                  fname="${person.name}"
                                  yearsInCompany="${person.yearsInCompany}"
                                  .photo="${person.photo}"
                                  profile="${person.profile}"
                                  @delete-person="${this.deletePerson}"
                                  @info-person="${this.infoPerson}"></persona-ficha-listado>`
                )}

            </main>
          </div>
          <div class="row">
            <persona-form
              id="personForm"
              class="d-none border rounded border-primary"
              @persona-form-close="${this.personFormClose}"
              @persona-form-store="${this.personaFormStore}"></persona-form>
          </div>

          <persona-main-dm @chargePeople=${this.chargePeople}></persona-main-dm>
      `
    }

    chargePeople(e) {
      console.log ("cargaPeople en persona-main");
      this.people = e.detail.people;
    }


    updated(changedProperties) {
      console.log("updated en persona-main");
      if (changedProperties.has("showPersonForm")){
        console.log("Ha cambiado el valor de la propiedad showPersonForm en persona-main");

        if (this.showPersonForm === true)
        {
          this.showPersonFormData();
        } else {
          this.showPersonList();
        }
      }

      if(changedProperties.has("people")){
        console.log("Ha cambiado el valor de la propiedad people en persona-main");

        this.dispatchEvent(
          new CustomEvent(
            "people-updated",
            {
              detail: {
                people: this.people
              }
            }
          )
        )
      }
    }

    deletePerson(e) {
      console.log ("deletePerson en persona-main");
      console.log ("Se va a borrar la persona de nombre" + e.detail.name);

      this.people = this.people.filter(
        person => person.name != e.detail.name
      );
    }

    infoPerson(e) {
      console.log ("infoPerson en persona-main");
      console.log ("Se ha pedido más información de la persona " + e.detail.name);

      let chosenPerson = this.people.filter(
        person => person.name === e.detail.name
      );

      let person = {};
      person.name = chosenPerson[0].name;
      person.profile = chosenPerson[0].profile;
      person.yearsInCompany = chosenPerson[0].yearsInCompany;

      this.shadowRoot.getElementById("personForm").person = person;
      this.shadowRoot.getElementById("personForm").editingPerson = true;
      this.showPersonForm = true;
    }

    showPersonList(){
      console.log("showPersonList");
      console.log("Mostrando el listado de las personas");

      this.shadowRoot.getElementById("peopleList").classList.remove("d-none");
      this.shadowRoot.getElementById("personForm").classList.add("d-none");
    }

    showPersonFormData(){
      console.log("showPersonFormData");
      console.log("Mostrando el formulario de personas");

      this.shadowRoot.getElementById("personForm").classList.remove("d-none");
      this.shadowRoot.getElementById("peopleList").classList.add("d-none");
    }

    personFormClose(e){
      console.log("personFormClose");

      this.showPersonForm = false;
    }

    personaFormStore(e){
      console.log("personaFormStore");
      console.log(e.detail.person);

      if (e.detail.editingPerson) {
        console.log("Se va a actualizar la persona de nombre " + e.detail.person.name);

        /*let indexOfPerson = this.people.findIndex(
          person => person.name === e.detail.person.name
        );

        if (indexOfPerson >= 0 ) {
          console.log("Persona encontrada");

          this.people[indexOfPerson] = e.detail.person;
        }*/

        this.people = this.people.map(
          person => person.name === e.detail.person.name
            ? person = e.detail.person : person);

      } else {
        console.log("Se va a almacenar una persona nueva ");
        //this.people.push(e.detail.person);
        this.people = [...this.people, e.detail.person]; //Spread syntax
        //this.people = [this.people, e.detail.person]; //Esto sería añadir array de array + 1 elemento
      }


      console.log("Persona almacenada");

      this.showPersonForm = false;

    }
}

customElements.define("persona-main", PersonaMain);
