import { LitElement, html } from 'lit';

class TestApi extends LitElement{

    static get properties(){
      return {
        movies :{type: Array}
      };
    }

    constructor(){
      super();

      this.movies = [];
      this.getMovieData();
    }

    render(){
        return html`
          ${this.movies.map(
            movie => html`<div>La película ${movie.title} fue dirigida por ${movie.director} </div>
            `
          )}
      `
    }

    getMovieData(){
      console.log("getMovieData");
      console.log("Obteniendo datos de las películas");

      let xhr = new XMLHttpRequest(); //el objecto httprequest para por 4 estados, 4 cuando ha hecho la petición y el servidor ha contestado

      //onload se ejecuta cuando el xhr se ha abierto/enviado
      xhr.onload = () => {
        if (xhr.status === 200) {
          console.log("Petición completada correctamente");

          let APIResponse = JSON.parse(xhr.responseText);
          //console.log(APIResponse);
          this.movies = APIResponse.results;
          console.log(this.movies);
        }
      }

      xhr.open("GET", "https://swapi.dev/api/films");
      xhr.send(); //Si se quiere enviar body, se envía en el send

      console.log("Fin de getMovieData");
    }

}

customElements.define("test-api", TestApi);
